import React from 'react';
import logo from './logo.svg';
import './App.css';
import NodeTest from './components/nodeTest';

const App: React.FC = () => {

  return ( 
    <div className="App">
      <NodeTest></NodeTest>
    </div>
  );
}

export default App;
